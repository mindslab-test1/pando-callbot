written by mjchae

# Pando-callbot
## 판도 콜봇 - maumswitch

```
python pando-callbot.py
nohup python pando-callbot.py &
```

## 설정
```
전화 번호 설정
cd /usr/local/freeswitch/conf/dialplan/public
vi 00_inbound_did.xml

<extension name="pando_callbot">
  <condition field="destination_number" expression="^(172|0708020855[0-7])$">

expression 부분을 바꾸면 됨
```

### LOG 파일 정보
```
cd logs
log-pando-callbot #당일 로그파일
log-pando-callbot.%Y%m%d #YYYY/MM/DD 로그파일

```
### sort_log.py 사용법

```
python sort_log.py --log logs/{LOG_FILE_NAME} --mode sort_thread

Thread 순으로 정렬.
```

```
python sort_log.py --log logs/{LOG_FILE_NAME} --mode find_thread --ln {Thread Number}
특정 번호의 Thread 만 찾기
```

### 
