#/usr/bin/env python
# -*- coding: utf-8 -*-
import time
import datetime
import re
import json
import requests
import urllib
import threading
import logging
import logging.handlers
import SocketServer
import threading
from ESL import *
#import text2int_kor
import error_correction_menu

# 와이즈넛 폴봇용 챗봇 서버 정보
#BASE_URL = 'http://211.39.140.44:8080'
# 한솔 챗봇용 서버 정보
#BASE_URL = 'http://isacorderbot.com:7100'
# 판도 서버 연동 정보
BASE_URL = 'http://ec2-15-165-33-153.ap-northeast-2.compute.amazonaws.com:7100'
#projectId = 'P000000001'
WEBHOOK_URL = 'http://127.0.0.1:5000/webhook'
LOG_FORMAT = "[%(asctime)-10s] (%(filename)s:%(lineno)d) %(levelname)s %(threadName)s - %(message)s"
ESL_HOST_IP = '127.0.0.1'
ESL_INBOUND_PORT = '8021'
ESL_PASSWORD = 'ClueCon'

logging.basicConfig(format=LOG_FORMAT)
logger = logging.getLogger("pando-callbot")
logger.setLevel(logging.INFO)

projectId = {'07080208550' : 'P000001011',
            '07080208551' : 'P000001012',
            '07080208552' : 'P000001015',
            '07080208553' : 'P000001016',
            '07080208554' : 'P000002001',
            '07080208555' : 'P000002002',
            '07080208556' : 'P000002003',
            '07080208557' : 'P000002004',
            '07080208558' : 'P000002011',
            '07080208559' : 'P000002012',
            '07080208560' : 'P000002013',
            '07080208561' : 'P000002014',
            '07080208562' : 'P000002015',
            '07080208563' : 'P000002016',
            '07074145484' : 'P000000001',
        }

file_handler = logging.handlers.TimedRotatingFileHandler(
        filename='./logs/log-pando-callbot', when='midnight', interval=1
        )
file_handler.suffix = '%Y%m%d'
logger.addHandler(file_handler)
file_handler.setFormatter(logging.Formatter(LOG_FORMAT))

class Pando_callbot_Server(object):
    def __init__(self, name, conn):
        self.name = name
        self.conn = conn
        logger = logging.getLogger("pando-callbot.{}".format(self.name))
        self.result = ''
        self.enable_chat = True
        self.rlist = []
        self.headers = \
        {
            'content-type':'application/json', 
            'charset':'utf-8',
        }
        self.aistart = False
        self.output = ''
        self.action = {}
        self.profile = ''
        self.store_dic = {}
        self.projectId = ''
        self.brain_stt = False
        self.google_stt = False
        self.timer = None
        self.playing_tts = False
        self.timer_def = None
        self.added_buf = ''

    def setup(self):
        self.info = self.conn.getInfo()
        #logger.info('getInfo: {}'.format(self.info.serialize('plain')))
        self.uuid = self.info.getHeader('caller-unique-id')
        """
        self.caller_id_number = self.info.getHeader('caller-caller-id-number')
        self.caller_id_name = self.info.getHeader('caller-caller-id-name')
        self.callstate = self.info.getHeader('channel-call-state')
        """
        evt = json.loads(self.info.serialize('json'))
        #logger.info('info: {}'.format(evt['variable_sip_h_Diversion']))
        #self.uuid = evt['Caller-Unique-ID']
        self.caller_id_number = evt['Caller-Caller-ID-Number']
        self.caller_id_name = evt['Caller-Caller-ID-Name']
        self.callstate = evt['Channel-Call-State']
        
        #variable_sip_h_Diversion = evt['variable_sip_h_Diversion']
        variable_sip_h_Diversion = self.info.getHeader('variable_sip_h_Diversion')
        if variable_sip_h_Diversion:
            logger.info('{}'.format(variable_sip_h_Diversion))
            diversion = re.search('[^<tel:, >]+', variable_sip_h_Diversion).group()
            self.dnis = diversion 
        else:
            self.dnis = '07080208550'
        
        logger.info('{}, {}, {}, {}'.format(self.uuid, self.caller_id_number, self.caller_id_name, self.callstate))
        self.conn.filter('unique-id', self.uuid)
        self.conn.events('plain', 'all')
        # 대화 시작
        self.conn.execute('ring_ready', '', self.uuid)
        self.conn.execute('sleep', '2000', self.uuid)
        self.conn.execute('answer', '', self.uuid)

    def open_dialog(self):
        if self.aistart == False: 
            url = BASE_URL + '/pandoivr/aistart/' + self.projectId 
        else:
            url = BASE_RUL + '/pandoivr/message/' + self.projectId
        logger.info('url: {}'.format(url))
        payload = \
        {
          'callid': self.uuid,
          'ani': self.caller_id_number,
          'dnis': self.dnis
        }
        response = requests.post(url, headers=self.headers, data=json.dumps(payload))
        logger.info('response: {}'.format(response))
        if self.aistart == False:
            resp = response.json()
            if resp['success'] == True:
                self.aistart = True
        logger.info('aistart: {}, ani: {}, dnis: {}'.format(self.aistart, self.caller_id_number, self.dnis))
        json.dumps(resp, ensure_ascii=False, encoding='utf8').encode('utf8')
        self.conn


    def close_dialog(self):
        url = BASE_URL + '/pandoivr/aiend/' + self.projectId
        payload = \
        {
          'callid': self.uuid
        }
        
        response = requests.post(url, headers=self.headers, data=json.dumps(payload))
        logger.info('response: {}'.format(response))
        resp = response.json()
        if self.aistart == True and resp['success'] == True:
            self.aistart = False
        logger.info('callid: {}, finish:{}'.format(self.uuid, resp['success']))
        self.conn.execute('hangup')

    def dialog_chat(self):
        #url = BASE_URL + '/api/iChatResponse'
        logger.info('uuid: {}'.format(self.uuid)) 
        url = BASE_URL + '/pandoivr/message/' + self.projectId
        payload = \
        {
          'callid': self.uuid,
          'input': self.result
        }

        response = requests.post(url, headers=self.headers, data=json.dumps(payload))
        #logger.info('headers: {}'.format(response.headers)) 
        resp = response.json()
        #json.dumps(resp, ensure_ascii=False).encode('utf-8')
        logger.info('response: {}'.format(resp)) 
        self.timestamp = (int)(time.time() * 1000000)
        self.datetime = datetime.datetime.fromtimestamp(self.timestamp/1000000.0) 
        logger.info('chat time: {}, {}'.format(self.timestamp, self.datetime))
        self.output = resp['output'].encode('utf-8')
        logger.info('[{}] output: {}'.format(self.uuid[:8], self.output))
        self.action = resp['action']
        logger.info('[{}] action_type: {}'.format(self.uuid[:8], self.action))
        if 'profile' in resp:
            self.profile = resp['profile']
            logger.info('profile: {}'.format(self.profile))

        if '\n' in self.output:
            temp_text = self.output.replace('\n', '')
            self.output = temp_text

        if '[매장번호]' in self.output:
            self.output = self.output.replace('[매장번호]\n', '')
            split_list = self.output.split('\n')
            #logger.info('split_list: {}'.format(split_list))
            result_string = ''
            dic_list = []
            for item in split_list:
                dic_list.append(item.split(': '))
                new_item = re.sub(r'[0-9]+', '', item)
                #logger.info('item: {}'.format(new_item))
                result_string = result_string + '. ' + new_item
            self.output = result_string
            dic_list.pop(0)
            self.store_dic = dict(dic_list)
            #logger.info('store_dic: {}'.format(self.store_dic))


        if self.profile == 'uni2_google':
            self.brain_stt_stop(self.uuid)
            self.google_stt_start(self.uuid)
        elif self.profile == 'uni2':
            self.google_stt_stop(self.uuid)
            self.brain_stt_start(self.uuid)
        

        if 'finish' in self.action['type']:
            logger.info('self.action[type]: {}'.format(self.action['type']))
            self.conn.setEventLock('1')
            self.conn.execute('speak', 'simple_tts|ko-KR|' + self.output, self.uuid)
            self.conn.setEventLock('0')
            self.close_dialog()
        elif 'agent' in self.action['type']:
            logger.info('[{}] self.action[type]: {}'.format(self.uuid[:8], self.action['type']))
            self.conn.setEventLock('1')
            self.conn.execute('speak', 'simple_tts|ko-KR|' + self.output, self.uuid)
            self.conn.setEventLock('0')
            self.conn.execute('set', 'hangup_after_bridge=true')
            #self.conn.execute('set', 'ignore_early_media=true')
            logger.info('self.action[number]: {}'.format(self.action['number']))
            self.conn.execute('set', 'effective_caller_id_number={}'.format(self.caller_id_number))
            self.conn.execute('bridge', '{sip_cid_type=rpid}sofia/gateway/dreamline/' + str(self.action['number']), self.uuid)
            #self.close_dialog()
        else:
            if self.playing_tts:
                self.conn.execute('break', '', self.uuid)
                logger.info('stop playng tts using break()')
            #logger.info('output: {}'.format(self.output))
            self.conn.execute('speak', 'simple_tts|ko-KR|' + self.output, self.uuid)            
            self.playing_tts = True
            
    def send_webhook(self, path, payload):
        url = WEBHOOK_URL + path
        response = requests.post(url, headers=self.headers, data=json.dumps(payload))
        #resp = response.json()
        #json.dumps(resp, ensure_ascii=False).encode('utf-8')
        logger.info('webhook response: {}'.format(response)) 

    def brain_stt_start(self, uuid):
        if self.brain_stt == False:
            stt_command = 'uuid_brain_stt ' + uuid + ' start ko-KR'
            logger.info('stt_command: {}'.format(stt_command))
            self.conn.bgapi(stt_command)
            self.brain_stt = True

    def brain_stt_stop(self, uuid):
        if self.brain_stt == True:
            stt_command = 'uuid_brain_stt ' + uuid + ' stop'
            logger.info('stt_command: {}'.format(stt_command))
            self.conn.bgapi(stt_command)
            self.brain_stt = False

    def google_stt_start(self, uuid):
        if self.google_stt == False:
            stt_command = 'uuid_google_transcribe ' + uuid + ' start ko-KR'
            logger.info('stt_command: {}'.format(stt_command))
            self.conn.bgapi(stt_command)
            self.google_stt = True

    def google_stt_stop(self, uuid):
        if self.google_stt == True:
            stt_command = 'uuid_google_transcribe ' + uuid + ' stop'
            logger.info('stt_command: {}'.format(stt_command))
            self.conn.bgapi(stt_command)
            self.google_stt = False

    def on_channel_park(self, e):
        self.code = e.getHeader('code')
        logger.debug('on_channel_park: code={}'.format(self.code))

    def on_channel_answer(self, e):
        if self.enable_chat:
            logger.info('~~~ answer channel a-leg ~~~')
            evt = json.loads(e.serialize('json'))
            self.caller_id_number = evt['Caller-Caller-ID-Number']
            self.caller_id_name = evt['Caller-Caller-ID-Name']
            logger.info('{}, {}, {}, {}'.format(self.uuid, self.caller_id_number, self.caller_id_name, self.callstate))
            self.projectId = projectId['{}'.format(self.callee)]
            self.open_dialog()
            # TODO: by shinwc
            #self.brain_stt_start(self.uuid)
            if self.aistart == True:
                self.dialog_chat()

    def on_channel_bridge(self, e):
        logger.info('~~~ bridge channel b-leg ~~~')
        evt = json.loads(e.serialize('json'))
        self.bleg_uuid = e.getHeader('other-leg-unique-id')
        self.bleg_callee_id_number = evt['Other-Leg-Callee-ID-Number']
        self.bleg_callee_id_name = evt['Other-Leg-Callee-ID-Name']
        logger.info('{}, {}, {}, {}'.format(self.bleg_uuid, self.bleg_callee_id_number, self.bleg_callee_id_name, self.callstate))
        self.enable_chat = False
        # TODO: by shinwc
        #self.brain_stt_start(self.bleg_uuid)
        self.conn.filter('unique-id', str(self.bleg_uuid))

    def on_channel_unbridge(self, e):
        logger.info('~~~ unbridge channel b-leg ~~~')

    def on_channel_hangup(self, e):
        uuid = e.getHeader('Unique-ID')
        # TODO: kill speak timer 
        if self.timer != None:
            if self.timer.isAlive():
                self.timer.cancel()
                self.timer.join()
                logger.info('~~~ speak timer killed! ~~~')

        if uuid == self.uuid:
            logger.info('~~~ channel a-leg hangup ~~~')
        elif uuid == self.bleg_uuid:
            logger.info('~~~ channel b-leg hangup ~~~')

    def send_custom_event(self, app, app_data):
        # TODO: speak timeout event 
        event = ESLevent("CUSTOM", "timeout::speak")
        event.addHeader('Unique-ID', self.uuid)
        event.addHeader('Application', app)
        event.addHeader('Application-Data', app_data)
        event.addHeader('result', 'timeout')
        self.conn.sendEvent(event)
        logger.info('send custom event(thru outbound): {}'.format(event))

    def send_custom_event_in(self, app, app_data, tmout=''):
        self.host = ESL_HOST_IP         #'127.0.0.1'
        self.port = ESL_INBOUND_PORT    #'8021'
        self.password = ESL_PASSWORD    # 'ClueCon'
        logger.info('init info: {} {} {}'.format(self.host, self.port, self.password))
        self.in_conn = ESLconnection(self.host, self.port, self.password)
        if self.in_conn:
            if tmout == '' :
                event = ESLevent("CUSTOM", "timeout::speak")
            else:
                event = ESLevent("CUSTOM", tmout) # "timeout::timer_def"
            event.addHeader('Unique-ID', self.uuid)
            event.addHeader('Application', app)
            event.addHeader('Application-Data', app_data)
            event.addHeader('result', 'timeout')
            self.in_conn.sendEvent(event)
            logger.info('send custom event(thru inbound): {}'.format(event))
            self.in_conn.disconnect()

    #@staticmethod
    def timer_callback(self, *args, **kwargs):
        app = args[0].encode('utf-8')
        app_data = args[1].encode('utf-8')
        logger.info('timer_callback args: {} {}'.format(app, app_data))
        #self.send_custom_event(app, app_data)
        self.send_custom_event_in(app, app_data)
        """
        if '해당 음성봇ID가 존재하지 않습니다. 관리자에게 문의바랍니다.' in self.output:
            logger.info('break reason : {}'.format(self.output))
            break
        if 'express 실행' in self.output:
            logger.ifo('break reason : {}'.format(self.output))
            break
        """
    def timer_def_callback(self, *args, **kwargs):
        app = args[0].encode('utf-8')
        app_data = args[1].encode('utf-8')
        logger.info('timer_callback args: {} {}'.format(app, app_data))
        #self.send_custom_event(app, app_data)
        self.send_custom_event_in(app, app_data, 'timeout::timer_def')

    def on_channel_execute_complete(self, e):
        evt = json.loads(e.serialize('json'))
        app = evt['Application']
        if app == 'speak': 
            #logger.info('event: {}'.format(e.serialize('plain'))) 
            app_data = evt['Application-Data'] 
            current_app = e.getHeader('variable_current_application') 
            term_digit = e.getHeader('variable_playback_terminator_used')
            hangup_disposition = e.getHeader('variable_sip_hangup_disposition')
            if current_app == 'break' or current_app == 'hangup':
                return 
            elif term_digit != None: 
                return
            elif hangup_disposition == 'recv_bye':
                return
            logger.info('current_app: {}, term_digit: {}'.format(current_app, term_digit))
            self.playing_tts = False
            self.timer = threading.Timer(interval=15.0, function=self.timer_callback, args=[app, app_data] ) 
            self.timer.start() # after x seconds, timer_callback will be executed
            logger.info('execute complete: {}'.format(app_data.encode('utf-8'))) 

    def on_timeout_speak(self, e):
        uuid = e.getHeader('Unique-ID')
        self.result = e.getHeader('result')
        logger.info('[{}] result: {}'.format(uuid[:8], self.result))
        self.dialog_chat()  

    def on_timeout_timer_def(self, e):
        uuid = e.getHeader('Unique-ID')
        self.result = e.getHeader('result')
        logger.info('[{}] timer_def: [{}]'.format(uuid[:8], self.added_buf))
        if len(self.added_buf) > 0 :
            self.result = self.added_buf
            self.added_buf = ''
            self.dialog_chat()  

    def on_custom(self, e):
        uuid = e.getHeader('Unique-ID')
        body = json.loads(e.getBody())
        text = body['text'].rstrip('\n').lstrip('\n')
        self.result = text.encode('utf-8')
        self.reuslt = self.result.lstrip()
        
        logger.info('[{}] result: {}, {}'.format(uuid[:8], self.result, self.datetime))
                  
        if '테스트' in self.output:
            #logger.info('store_dic: {}'.format(self.store_dic))
            fixed_result = error_correction_menu.correction(self.result, list(self.store_dic.keys()))
            self.result = self.store_dic[fixed_result]
            logger.info('store_number: {}'.format(self.result))

        
        # TODO: kill speak timer 
        if self.timer != None:
            if self.timer.isAlive():
                self.timer.cancel()
                self.timer.join()
                logger.info('~~~ speak timer killed! ~~~')

        self.result_and_chat(e)
        logger.debug('--- End of on_custom() ---')

    def result_and_chat(self, e):
        evt = json.loads(e.serialize('json'))
        if self.timer_def != None:
            if self.timer_def.isAlive():
                self.timer_def.cancel()
                self.timer_def.join()
                logger.info('~~~ speak timer_def killed! ~~~')
        if True:
            if(len(self.result) > 0) :
                if len(self.added_buf) > 0 :
                    self.added_buf = self.added_buf + ' ' + self.result
                else:
                    self.added_buf = self.result
            self.result = ''
            if 'Application' in evt:
                app = evt['Application']
            else:
                app = 'app'
            if 'Application-Data' in evt:
                app_data = evt['Application-Data']
            else:
                app_data = 'app_data'
            self.timer_def = threading.Timer(interval=3.0, function=self.timer_def_callback, args=[app, app_data] ) 
            self.timer_def.start() # after x seconds, timer_callback will be executed
        else:
            if self.enable_chat:
                if len(self.result):
                    self.dialog_chat()  

    def on_custom2(self, e):
        uuid = e.getHeader('Unique-ID')
        body = json.loads(e.getBody())
        logger.info('google event body: {}'.format(body))
        text = body['alternatives'][0]['transcript'].rstrip('\n').lstrip('\n')
        self.result = text.encode('utf-8')
        self.reuslt = self.result.lstrip()
        
        logger.info('[{}] result: {}, {}'.format(uuid[:8], self.result, self.datetime))
        
        # TODO: kill speak timer 
        if self.timer != None:
            if self.timer.isAlive():
                self.timer.cancel()
                self.timer.join()
                logger.info('~~~ speak timer killed! ~~~')

        self.result_and_chat(e)
        logger.debug('--- End of on_custom2() ---')

    def on_dtmf(self, e):
        logger.debug('on_dtmf: {}'.format(e.serialize('plain')))
        uuid = e.getHeader('Unique-ID')
        dtmf = e.getHeader('DTMF-Digit')
        logger.info('on_dtmf(), DTMF-Digit: {}'.format(dtmf))
        # TODO: kill speak timer 
        if self.timer != None:
            if self.timer.isAlive():
                self.timer.cancel()
                self.timer.join()
                logger.info('~~~ speak timer killed! ~~~')
        #if self.enable_chat == False:
        #    if dtmf == '1' or dtmf == '2':
                #self.conn.execute('hangup', '', self.uuid)
                #self.conn.execute('deflect', 'sip:{}@61.103.31.26'.format(self.action['number']), self.uuid)
        logger.debug('--- End of on_dtmf() ---')

    def process_event(self):
        while self.conn.connected:
            e = self.conn.recvEvent()
            #e = self.conn.recvEventTimed(1)
            #if e == None:
            #    continue
            evt = json.loads(e.serialize('json'))
            event_name = evt['Event-Name'].lower()
            self.timestamp = e.getHeader('Event-Date-Timestamp')
            if self.timestamp !=  None:
                self.datetime = datetime.datetime.fromtimestamp((int)(self.timestamp)/1000000.0) 
            caller = e.getHeader('Caller-Username')
            callee = e.getHeader('Caller-Destination-Number')
            if caller and callee:
                self.caller = caller
                self.callee = callee
            #method_name = 'on_%s' % event_name
            #logger.info('method_name: {}'.format(method_name))
            if event_name == 'server_disconnected':
                self.disconnected = True
                logger.info('disconnected!')
                break
            elif event_name == 'channel_park':
                logger.info('on_channel_park()')
                self.on_channel_park(e)
            elif event_name == 'channel_answer':
                self.on_channel_answer(e)
            elif event_name == 'channel_hangup':
                self.on_channel_hangup(e)
                self.conn.disconnect()
                break
            elif event_name == 'channel_bridge':
                self.on_channel_bridge(e)
                logger.info('on_channel_brdige, break')
                break
            elif event_name == 'channel_unbridge':
                self.on_channel_unbridge(e)
            elif event_name == 'channel_callstate':
                self.callstate = evt['Channel-Call-State']
                logger.info('callstate: {}'.format(self.callstate))
            elif event_name == 'channel_state':
                self.state = evt['Channel-State']
                logger.info('state: {}'.format(self.state))
            if event_name == 'dtmf':
                self.on_dtmf(e)
            elif event_name == 'custom':
                subclass = evt['Event-Subclass'].lower()
                logger.debug('subclass: {}'.format(subclass))
                if subclass == 'brain_stt::transcription':
                    self.on_custom(e)
                elif subclass == 'google_transcribe::transcription':
                    self.on_custom2(e)
                elif subclass == 'timeout::speak':
                    self.on_timeout_speak(e)
                elif subclass == 'timeout::timer_def':
                    self.on_timeout_timer_def(e)
                else:
                    logger.info('{} custom!'.format(event_name))

            elif event_name == 'channel_execute_complete':
                self.on_channel_execute_complete(e)
            else:
                pass
        logger.info('>>>>> connection closed <<<<<')

class ESLRequestHandler(SocketServer.BaseRequestHandler):
    def setup(self):
        logger.info('{} connected!'.format(self.client_address))

        fd = self.request.fileno()
        logger.debug('fd: {}'.format(fd))

        con = ESLconnection(fd)
        logger.debug('Connected: {}'.format(con.connected))
        logger.debug('Thread Name:{}'.format(threading.current_thread().name))
        name = threading.current_thread().name
        if con.connected():
            pando_callbot = Pando_callbot_Server(name, con)
            pando_callbot.setup()
            pando_callbot.process_event()

#server host is a tuple ('host', port)
server = SocketServer.ThreadingTCPServer(('127.0.0.1', 8048), ESLRequestHandler)
server.serve_forever()
