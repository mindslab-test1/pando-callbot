#-*- coding: utf-8 -*-

import re
import argparse
import os

time_pattern = "(\[[\d]{4}-[\d]{2}-[\d]{2} [\d]{2}:[\d]{2}:[\d]{2},[\d]{3}\])"
tm_p = re.compile(time_pattern)

parser = argparse.ArgumentParser(description='Process some integers.')
parser.add_argument('--log', required=True)
parser.add_argument('--mode', required=True, help='find_thread, sort_thread')
parser.add_argument('--ln', required=False, type=int, help='type line number')

args = parser.parse_args()
dir = os.getcwd()+'/'

# 로그 내역 전처리 (띄어쓰기 오류)
def init_log(logs):
    for log in logs:
        if tm_p.search(log) == None:
            index = logs.index(log)   
            logs[index-1] = logs[index-1].strip()+" "+log.strip()
            logs.pop(index)
    
    return "\n".join(logs)

# 번호로 스레드 찾기
def find_thread(file, thread_num):
    thread_name = "Thread-"+str(thread_num)
    pattern = "(\[[\d]{4}-[\d]{2}-[\d]{2} [\d]{2}:[\d]{2}:[\d]{2},[\d]{3}\][-:. \d\w()]*"+thread_name+"[-:;.',/<>=~?\"()\[\]{}!\d\w \\가-힣]*)"
    p = re.compile(pattern)
    result = []
    with open(file, "r", encoding='utf-8') as f:
        logs = f.readlines()
        logs = init_log(logs)
        
        result = p.findall(logs) # 스레드 번호 일치하는 것만 모아두기

    # 파일 출력
    output = dir+str(thread_num)+file

    with open(output, "w") as f:
        for thread in result:
            f.write(thread.strip()+"\n")

    return result

# 스레드 순석대로 정렬하기 (시간순 + 스레드순)
def sort_thread(file):
    thread_pattern = "(\[[\d]{4}-[\d]{2}-[\d]{2} [\d]{2}:[\d]{2}:[\d]{2},[\d]{3}\][-:;.',/<>=~?\"()\[\]{}!\d\w \\가-힣]*)"
    thread_name_pattern = "(Thread\-[\d]+)"
    start_pattern = "(\[[\d]{4}-[\d]{2}-[\d]{2} [\d]{2}:[\d]{2}:[\d]{2},[\d]{3}\][-:. \d\w()]*Thread-1 [-:;.',/<>=~?\"()\[\]{}!\d\w \\가-힣]*connected!)"

    p = re.compile(thread_pattern)
    th_p = re.compile(thread_name_pattern)
    st_p = re.compile(start_pattern)

    threads = []
    with open(file, "r") as f:
        logs = f.readlines()
        logs = init_log(logs)

        threads = p.findall(logs)
        threads.sort(key=lambda x:tm_p.findall(x)[0]) # 시간 순서대로 정렬
        
        term = []
        result = []
        for thread in threads:
            if st_p.search(thread) != None: # 번호 초기화 시작되기 직전까지 다 모았으면
                term.sort(key=lambda x:int(th_p.findall(x)[0].split('-')[1])) # 스레드 순서대로 정렬
                result.append(term)
                term = []
            term.append(thread)
        if len(term) > 0:
            result.append(term)

    # 파일 출력
    output = dir+file+'_sorted'
   
    with open(output, "w") as f:
         for threads in result:
             for thread in threads:
                f.write(thread+"\n")

    return result

if __name__ == "__main__":
    if args.mode == 'sort_thread':
        sort_thread(args.log)
    if args.mode == 'find_thread':    
        find_thread(args.log, args.ln)
