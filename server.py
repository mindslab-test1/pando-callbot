#/usr/bin/env python
# -*- coding: utf-8 -*-
import time
import datetime
import re
import json
import requests
import urllib
import threading
import logging
import SocketServer
from ESL import *
import car_plate

# 와이즈넛 폴봇용 챗봇 서버 정보
BASE_URL = 'http://211.39.140.44:8080'
WEBHOOK_URL = 'http://127.0.0.1:5000/webhook'
LOG_FORMAT = "[%(asctime)-10s] (%(filename)s:%(lineno)d) %(levelname)s %(threadName)s - %(message)s"

logging.basicConfig(format=LOG_FORMAT)
logger = logging.getLogger("polbot")
logger.setLevel(logging.INFO)

class PolbotServer(object):
    def __init__(self, name, conn):
        self.name = name
        self.conn = conn
        logger = logging.getLogger("polbot.{}".format(self.name))
        self.timeout = 5
        self.intent = 'greeting'
        self.result = 'hi'
        self.enable_chat = True
        self.rlist = []
        self.headers = \
        {
            'content-type':'application/json', 
            'charset':'utf-8',
        }

    def setup(self):
        self.info = self.conn.getInfo()
        self.uuid = self.info.getHeader('caller-unique-id')
        """
        self.caller_id_number = self.info.getHeader('caller-caller-id-number')
        self.caller_id_name = self.info.getHeader('caller-caller-id-name')
        self.callstate = self.info.getHeader('channel-call-state')
        """
        evt = json.loads(self.info.serialize('json'))
        #self.uuid = evt['Caller-Unique-ID']
        self.caller_id_number = evt['Caller-Caller-ID-Number']
        self.caller_id_name = evt['Caller-Caller-ID-Name']
        self.callstate = evt['Channel-Call-State']
        #logger.debug('info: {}'.format(evt))
        logger.info('{}, {}, {}, {}'.format(self.uuid, self.caller_id_number, self.caller_id_name, self.callstate))
        self.conn.filter('unique-id', self.uuid)
        #self.conn.sendRecv("".format(self.uuid))
        #self.conn.sendRecv("divert_events on")
        self.conn.events('plain', 'all')
        # 대화 시작
        self.conn.execute('ring_ready', '', self.uuid)
        self.conn.execute('sleep', '2000', self.uuid)
        self.conn.execute('answer', '', self.uuid)

    def open_dialog(self):
        url = BASE_URL + '/api/sessionRequest'
        response = requests.post(url, headers=self.headers)
        resp = response.json()
        json.dumps(resp, ensure_ascii=False, encoding='utf8').encode('utf8')

        self.key = resp['sessionKey']
        logger.info('sessionKey: {}'.format(self.key))

    def dialog_chat(self):
        url = BASE_URL + '/api/iChatResponse'
        payload = \
        {
          'projectId':'713a66231ba5',
          'query':'%s' % self.result,
          'sessionKey':'%s' % self.key,
          'process':'%s' % self.intent
        }

        response = requests.post(url, headers=self.headers, params=payload)
        #logger.info('headers: {}'.format(response.headers)) 
        resp = response.json()
        #json.dumps(resp, ensure_ascii=False).encode('utf-8')
        #logger.info('response: {}'.format(resp)) 
        self.timestamp = (int)(time.time() * 1000000)
        self.datetime = datetime.datetime.fromtimestamp(self.timestamp/1000000.0) 
        logger.info('chat time: {}, {}'.format(self.timestamp, self.datetime))
        self.answer = resp['answer']['message'].encode('utf-8')
        self.intent = resp['answer']['process']
        logger.info('answer: {}'.format(self.answer)) 
        logger.info('intent: {}'.format(self.intent)) 
        self.conn.execute('break', '', self.uuid)
        if 'scenario_end' in self.intent:
            self.conn.setEventLock('1')
            self.conn.execute('speak', 'simple_tts|ko-KR|' + self.answer, self.uuid)
            if self.result in ['상담사', '상담원', '상담', '상담사 연결', '상담사 연결해줘', '상담원 연결해줘', '상담사 연결해 줘', '상담원 연결해 줘']:
                #self.conn.execute('transfer', '5724', self.uuid)
                #self.conn.execute('bridge', 'sofia/external/5725%221.168.32.165', self.uuid)
                self.conn.execute('bridge', 'sofia/external/01027528717@203.251.164.202', self.uuid)
            elif '문자' in self.result:
                self.conn.execute('hangup')
            self.conn.setEventLock('0')
        else:
            self.conn.execute('speak', 'simple_tts|ko-KR|' + self.answer, self.uuid)
        payload = \
        {
          'uuid':'%s' % self.uuid,
          'answer':'%s' % self.answer,
          'intent':'%s' % self.intent,
          'caller':'%s' % self.caller,
          'callee':'%s' % self.callee,
          'isConsultant':'%s' % 'False' if self.enable_chat else 'True',
          #'isConsultant':'%s' % self.enable_chat,
          'timestamp':'%d' % self.timestamp,
          'datetime':'%s' % self.datetime
        }
        logger.debug('payload: {}'.format(payload))
        self.send_webhook('/chat', payload)

    def send_webhook(self, path, payload):
        url = WEBHOOK_URL + path
        response = requests.post(url, headers=self.headers, data=json.dumps(payload))
        #resp = response.json()
        #json.dumps(resp, ensure_ascii=False).encode('utf-8')
        logger.info('webhook response: {}'.format(response)) 

    def on_channel_park(self, e):
        self.code = e.getHeader('code')
        logger.debug('on_channel_park: code={}'.format(self.code))

    def on_channel_answer(self, e):
        if self.enable_chat:
            logger.info('~~~ answer channel a-leg ~~~')
            evt = json.loads(e.serialize('json'))
            self.caller_id_number = evt['Caller-Caller-ID-Number']
            self.caller_id_name = evt['Caller-Caller-ID-Name']
            logger.info('{}, {}, {}, {}'.format(self.uuid, self.caller_id_number, self.caller_id_name, self.callstate))
            self.open_dialog()
            stt_command = 'uuid_brain_stt ' + self.uuid + ' start ko-KR'
            logger.info('stt_command: {}'.format(stt_command))
            self.conn.bgapi(stt_command) 
            self.dialog_chat()

    def on_channel_bridge(self, e):
        logger.info('~~~ bridge channel b-leg ~~~')
        evt = json.loads(e.serialize('json'))
        #logger.info('bridge: {}'.format(e.serialize('plain')))
        self.bleg_uuid = e.getHeader('other-leg-unique-id')
        self.bleg_callee_id_number = evt['Other-Leg-Callee-ID-Number']
        self.bleg_callee_id_name = '상담사'
        #self.bleg_callee_id_name = evt['Other-Leg-Callee-ID-Name']
        logger.info('{}, {}, {}, {}'.format(self.bleg_uuid, self.bleg_callee_id_number, self.bleg_callee_id_name, self.callstate))
        self.enable_chat = False
        #self.bleg_uuid = evt['Other-Leg-Unique-ID']
        stt_command = 'uuid_brain_stt ' + str(self.bleg_uuid) + ' start ko-KR'
        logger.info('stt_command: {}'.format(stt_command))
        self.conn.bgapi(stt_command) 
        self.conn.filter('unique-id', str(self.bleg_uuid))

    def on_channel_unbridge(self, e):
        evt = json.loads(e.serialize('json'))
        #logger.info('bridge: {}'.format(e.serialize('plain')))
        self.enable_chat = True
        self.intent = 'greeting'
        self.result = 'hi'
        logger.info('Polbot dialog restarting...')
        self.conn.execute('break', '', self.uuid)
        self.dialog_chat()
        #self.bleg_uuid = evt['Other-Leg-Unique-ID']
        logger.debug('--- End of on_channel_unbridge() ---')

    def on_channel_hangup(self, e):
        uuid = e.getHeader('Caller-Unique-ID')
        if uuid == self.uuid:
            logger.info('~~~ channel a-leg hangup ~~~')
        elif uuid == self.bleg_uuid:
            logger.info('~~~ channel b-leg hangup ~~~')

    def on_channel_execute_complete(self, e):
        evt = json.loads(e.serialize('json'))
        app = evt['Application']
        #app_data = evt['Application-Data'] 
        logger.info('execute complete: {}'.format(app)) 

    def on_custom(self, e):
        uuid = e.getHeader('Unique-ID')
        #logger.info('uuid: {}'.format(uuid))

        body = json.loads(e.getBody())
        text = body['text'].rstrip('\n').lstrip('\n')
        """
        self.result = text.encode('utf-8')
        if '차량번호' in self.answer:
            self.result = '39라 5511'
        """
        self.result = text.encode('utf-8')
        if '차량번호' in self.answer:
            logger.info('result: {} for car number'.format(self.result))
            car_number = [car_plate.kor2num(index, item) for index, item in enumerate(text.split(' '))]
            logger.info('car number: {}'.format(''.join(car_number)))
            self.result = ''.join(car_number)

        payload = \
        {
          'uuid':'%s' % uuid,
          'result':'%s' % self.result,
          'timestamp':'%s' % self.timestamp,
          'datetime':'%s' % self.datetime,
          'caller':'%s' % self.caller,
          'callee':'%s' % self.callee,
          'isConsultant':'%s' % 'False' if self.enable_chat else 'True',
          'callerIdNumber':'%s' % self.caller_id_number if uuid == self.uuid else self.bleg_callee_id_number,
          'callerIdName':'%s' % self.caller_id_name if uuid == self.uuid else self.bleg_callee_id_name
        }
        logger.debug('payload: {}'.format(payload))
        self.send_webhook('/stt', payload)
        logger.info('[{}] result: {}, {}'.format(uuid[:8], self.result, self.datetime))
        wakeup = ['일팔이', '폴봇', '하이 폴봇', '헤이 폴봇', '일팔이 연결', '일팔이 연결해 주세요']
        if self.enable_chat:
            if len(self.result):
                self.dialog_chat() 
        elif self.result in wakeup:
            self.conn.setEventLock('1')
            command = 'uuid_kill ' + str(self.bleg_uuid)
            logger.info('command: {}'.format(command))
            self.conn.api(command) 
            self.enable_chat = True
            self.conn.execute('speak', 'simple_tts|ko-KR|다시 폴봇 상담을 시작합니다', self.uuid)
            self.conn.setEventLock('0')
            #self.intent = 'greeting'
            #self.result = 'hi'
            #self.dialog_chat() 
        self.rlist = []
        logger.debug('--- End of on_custom() ---')

    def process_callstate(self, e):
        uuid = e.getHeader('Unique-ID')
        payload = \
        {
            'uuid':'%s' % self.uuid,
            'caller':'%s' % self.caller,
            'callee':'%s' % self.callee,
            'callstate':'%s' % self.callstate,
            'datetime':'%s' % self.datetime,
        }
        logger.info('callstate: {}'.format(str(payload)))
        self.send_webhook('/callstate', payload)

    def alert_disconnection(self):
        payload = \
        {
            'disconnected':self.disconnected,
            'uuid':'%s' % self.uuid,
            'caller':'%s' % self.caller,
            'callee':'%s' % self.callee,
            'datetime':'%s' % self.datetime,
        }
        logger.info('disconnection payload: {}'.format(str(payload)))
        self.send_webhook('/disconnection', payload)


    def process_event(self):
        while self.conn.connected:
            e = self.conn.recvEvent()
            evt = json.loads(e.serialize('json'))
            event_name = evt['Event-Name'].lower()
            self.timestamp = e.getHeader('Event-Date-Timestamp')
            if self.timestamp !=  None:
                self.datetime = datetime.datetime.fromtimestamp((int)(self.timestamp)/1000000.0) 
            caller = e.getHeader('Caller-Username')
            callee = e.getHeader('Caller-Destination-Number')
            if caller and callee:
                self.caller = caller
                self.callee = callee
            method_name = 'on_%s' % event_name
            #logger.info(method_name)
            if event_name == 'server_disconnected':
                self.disconnected = True
                self.alert_disconnection()
                logger.info('disconnected!')
                break
            if event_name == 'channel_park':
                logger.info('on_channel_park()')
                self.on_channel_park(e)
            if event_name == 'channel_answer':
                self.on_channel_answer(e)
            if event_name == 'channel_hangup':
                self.on_channel_hangup(e)
            if event_name == 'channel_bridge':
                self.on_channel_bridge(e)
            if event_name == 'channel_unbridge':
                self.on_channel_unbridge(e)
            if event_name == 'channel_callstate':
                self.callstate = evt['Channel-Call-State']
                self.process_callstate(e)
                logger.info('callstate: {}'.format(self.callstate))
            if event_name == 'channel_state':
                self.state = evt['Channel-State']
                logger.info('state: {}'.format(self.state))
            if event_name == 'custom':
                subclass = evt['Event-Subclass'].lower()
                logger.debug('subclass: {}'.format(subclass))
                if subclass == 'brain_stt::transcription':
                    self.on_custom(e)
            if event_name == 'channel_execute_complete':
                self.on_channel_execute_complete(e)

class ESLRequestHandler(SocketServer.BaseRequestHandler):
    def setup(self):
        logger.info('{} connected!'.format(self.client_address))

        fd = self.request.fileno()
        logger.debug('fd: {}'.format(fd))

        con = ESLconnection(fd)
        logger.debug('Connected: {}'.format(con.connected))
        logger.debug('Thread Name:{}'.format(threading.current_thread().name))
        name = threading.current_thread().name
        if con.connected():
            polbot = PolbotServer(name, con)
            polbot.setup()
            polbot.process_event()

#server host is a tuple ('host', port)
server = SocketServer.ThreadingTCPServer(('127.0.0.1', 8040), ESLRequestHandler)
server.serve_forever()
